#encoding=utf-8
import urllib
import requests
import json
from crawler import crawler
def getIdx(board):
    thisurl = "https://www.ptt.cc/bbs/" + board + "/index.html"
    resp = requests.get(
                    url=thisurl,
                    cookies={'over18': '1'}, verify=False
                )
    html = resp.text.encode("utf-8")
    last_page_idx = html.find("上頁")
    start_idx = html.rfind("index", 0, last_page_idx)
    end_idx = html.rfind(".html", 0, last_page_idx)
    return int(html[start_idx + 5:end_idx]) + 1
def getArticles(board):
    idx = getIdx(board)
    return crawler(board, idx - 10, idx)
getArticles("Gossiping")
