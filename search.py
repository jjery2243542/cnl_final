from getArticles import getArticles
import time
def search(query, board):
    result = []
    tic = time.time()
    articles = getArticles(board)
    toc = time.time()
    print toc -tic
    for article in articles:
        if "messages" in article:
            cnt = 0
            for m in article["messages"]:
                push_content = m["push_content"].encode("utf-8")
                if push_content.find(query) != -1:
                    cnt += 1
            if cnt > 0:
                result.append((cnt, article["article_title"].encode("utf-8"), article["link"].encode("utf-8")))
    return result
result = search("QQ", "Gossiping")
for r in result:
    print r[1]
        
